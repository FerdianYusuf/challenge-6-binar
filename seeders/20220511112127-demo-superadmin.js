const bcrypt = require("bcrypt");
const { ROLES } = require("../lib/const");
const SALT_ROUND = 10;

("use strict");

module.exports = {
  async up(queryInterface, Sequelize) {
    const hashedPassword = await bcrypt.hash("ferdian123", SALT_ROUND);

    await queryInterface.bulkInsert(
      "users",
      [
        {
          name: "laluferdianyusuf",
          email: "superadmin@gmail.com",
          password: hashedPassword,
          role: ROLES.SUPERADMIN,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("users", null, {});
  },
};
